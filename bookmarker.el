(defvar bookmarker-filename "~/bookmarks.txt"
  "File in which to store (and from which to read) bookmarks.")

(defvar bookmarker-history '()
  "Visited urls.")

(defvar bookmarker-bookmark-cache '()
  "Cache of the current bookmarks. Used internally and filled
  with `bookmarker-get-bookmarks'")

(defvar bookmarker-complete-function 'completing-read
  "Completion function used to complete bookmark list")

(defun bookmarker-get-bookmarks (&optional bookmark-file)
  "Parse the bookmarks file and return an alist of name - url matches."
  (let ((bookmark-file (or bookmark-file bookmarker-filename))
        (bookmarks '()))
    (save-window-excursion
      (find-file bookmark-file)
      (goto-char (point-min))
      (while (re-search-forward "\"\\(.+?\\)\"[[:blank:]]+\\(.*\\)" nil t)
        (let ((name (match-string 1))
              (url (match-string 2)))
          (push (cons name url) bookmarks))))
    (reverse bookmarks)))

(defun bookmarker-expand-url (name &optional bookmark-file)
  "Expand and prompt for placeholders in url referenced by
NAME. Returns the full url. If the first argument is left blank
then the url returned will only be the domain section of a url."
  (interactive)
  (catch 'url
    (let* ((url (cdr (assoc name (bookmarker-get-bookmarks bookmark-file)))))
      (when url
        (let ((urlobj (url-generic-parse-url url))
              (match-start 0)
              (count 0))
          (while (setq match-start
                       (string-match "%{\\(.+?\\)}" url match-start))
            (let ((replacement (save-match-data
                                 (url-hexify-string
                                  (read-from-minibuffer
                                   (concat "String " (match-string 1 url) ": "))))))
              (when (and (eq count 0) (eq replacement ""))
                (throw 'url (url-host urlobj)))
              (setq count (1+ count))
              (setq url (replace-match replacement nil t url))
              ;; make sure the next replacement happens ahead of the last
              (setq match-start (+ match-start (length replacement)))))
          (throw 'url url))))))

(defun bookmarker-names (&optional bookmark-file)
  "All of the friendly names for bookmarks."
  (let ((bookmarks (bookmarker-get-bookmarks bookmark-file)))
    (mapcar 'car bookmarks)))

(defun bookmarker-visit ()
  "Ask for a url (or a name) and then visit it."
  (interactive)
  (let* ((name (funcall bookmarker-complete-function "Bookmark: "
                        (bookmarker-names) nil nil nil 'bookmarker-history))
         (url (bookmarker-expand-url name)))
    (browse-url (or url name))))

(provide 'bookmarker)
